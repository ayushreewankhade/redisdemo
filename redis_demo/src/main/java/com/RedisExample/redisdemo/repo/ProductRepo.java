package com.RedisExample.redisdemo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.RedisExample.redisdemo.entity.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

}
