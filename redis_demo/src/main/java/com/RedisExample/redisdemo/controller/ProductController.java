package com.RedisExample.redisdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.RedisExample.redisdemo.entity.Product;
import com.RedisExample.redisdemo.service.ProductService;

@Controller
public class ProductController {

	@Autowired
	ProductService service;
	
	@PostMapping("/save")
    public Product saveInvoice(@RequestBody Product product) {
       return service.saveProduct(product);
    }

    @GetMapping("/all") 
    public ResponseEntity<List<Product>> getAllInvoices(){
       return ResponseEntity.ok(service.getAllProducts());
    }

    @GetMapping("/getOne/{productId}")
    public Product getOneInvoice(@PathVariable Integer productId) {
       return service.getOneProduct(productId);
    }

    @PutMapping("/modify/{productId}")
    public Product updateInvoice(@RequestBody Product product, @PathVariable Integer productId) {
       return service.updateProduct(product, productId);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteInvoice(@PathVariable Integer productId) {
       service.deleteProduct(productId);
       return "Employee with id: "+productId+ " Deleted !";
    }
}
