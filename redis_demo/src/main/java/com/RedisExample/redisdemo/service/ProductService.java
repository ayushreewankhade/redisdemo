package com.RedisExample.redisdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.RedisExample.redisdemo.ProductNotFOundException;
import com.RedisExample.redisdemo.entity.Product;
import com.RedisExample.redisdemo.repo.ProductRepo;

@Service
public class ProductService {

	@Autowired
	private ProductRepo repo;
	
    public Product saveProduct(Product product) {

        return repo.save(product);
    }

    @CachePut(value="Product", key="#productId")
    public Product updateProduct(Product product, Integer productId) {
    	Product pro = repo.findById(productId)
            .orElseThrow(() -> new ProductNotFOundException("Invoice Not Found"));
       pro.setProductName(product.getProductName());
       pro.setPrice(product.getPrice());
       return  repo.save(pro);
    }

    @CacheEvict(value="Product", key="#productId")
    // @CacheEvict(value="Invoice", allEntries=true) //in case there are multiple records to delete
    public void deleteProduct(Integer productId) {
       Product product = repo.findById(productId)
           .orElseThrow(() -> new ProductNotFOundException("Invoice Not Found"));
       repo.delete(product);
    }
	
    @Cacheable(value="Product", key="#productId")
    public Product getOneProduct(Integer productId) {
    	Product product = repo.findById(productId)
         .orElseThrow(() -> new ProductNotFOundException("Invoice Not Found"));
       return product;
    } 
    
    @Cacheable(value="Product")
    public List<Product> getAllProducts() {
       return repo.findAll();
    }
}
